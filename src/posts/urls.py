from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _

import posts.views


urlpatterns = [
        url(_(r'^PostCreation/$'),
            posts.views.PostCreationView.as_view(),
            name='PostCreation'),
        url(_(r'^GetAllPosts/$'),
            posts.views.GetAllPostsView.as_view(),
            name='GetPosts'),
        url(_(r'^GetAllQuestions/$'),
            posts.views.GetAllQuestionsView.as_view(),
            name='GetQuestions'),


]